﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ShowControlSignals))]
public class ControlSignalsEditor : Editor
{
	public override void OnInspectorGUI ()
	{
		DrawDefaultInspector ();

		ShowControlSignals t = target as ShowControlSignals;
		if (t != null && t.inputs != null) {
			EditorGUILayout.Slider ("throttle", t.throttle, -1, 1);
			EditorGUILayout.Slider ("roll", t.roll, -1, 1);
			EditorGUILayout.Slider ("pitch", t.pitch, -1, 1);
			EditorGUILayout.Slider ("yaw", t.yaw, -1, 1);
			
			Repaint();
		}
	}
}
