﻿using UnityEngine;
using System.Collections;

public abstract class AbstractFilter : ControlSignals {
	
	public ControlSignals inputs;

	protected abstract Vector4 Outputs {
		get;
	}

	public Vector4 GetOutputs() {
		return Outputs;
	}

	public override float throttle {
		get { return Outputs.x; }
	}
	public override float roll {
		get { return Outputs.y; }
	}
	public override float pitch {
		get { return Outputs.z; }
	}
	public override float yaw {
		get { return Outputs.w; }
	}
}
