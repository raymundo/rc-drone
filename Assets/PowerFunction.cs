﻿using UnityEngine;
using System.Collections;

public class PowerFunction : AbstractFilter {

	public float ExponentThrottle = 3.0f;
	public float ExponentRoll = 3.0f;
	public float ExponentPitch = 3.0f;
	public float ExponentYaw = 3.0f;

	protected override Vector4 Outputs {
		get {
			return new Vector4(
			Mathf.Pow(inputs.throttle, ExponentThrottle),
			Mathf.Pow(inputs.roll, ExponentRoll),
			Mathf.Pow(inputs.pitch, ExponentPitch),
			Mathf.Pow(inputs.yaw, ExponentYaw)
			);
		}
	}
}
