﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(HingeJoint))]
[RequireComponent(typeof(ConstantForce))]
public class ControlHingeMotor : MonoBehaviour
{
	public ControlSignalsMultiplexer controlSignals;
	public Vector4 signalWeights;
	public float AxisScale = 0.1f;
	private float baseVelocity;
	private Vector3 baseRelativeForce;
	
	void Start ()
	{
		baseVelocity = hingeJoint.motor.targetVelocity;
		baseRelativeForce = constantForce.relativeForce;
	}

	void FixedUpdate ()
	{
		float inputValue = Vector4.Dot(controlSignals.GetOutputs(), signalWeights);
		float targetValue = inputValue * AxisScale + 1.0f;
		
		Vector3 myUp = transform.InverseTransformDirection(Vector3.up);
		//float angleToUp = Vector3.Angle(myUp, Vector3.up);

		JointMotor motor = hingeJoint.motor;
		motor.targetVelocity = targetValue * baseVelocity;
		hingeJoint.motor = motor;
		constantForce.relativeForce = targetValue * baseRelativeForce * myUp.y;
	}
}
