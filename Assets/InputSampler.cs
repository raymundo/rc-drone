﻿using UnityEngine;
using System.Collections;

public class InputSampler : ControlSignals {
	
	public string AxisNameThrottle;
	public string AxisNameRoll;
	public string AxisNamePitch;
	public string AxisNameYaw;
	
	private Vector4 values;

	public override float throttle {
		get { return values.x; }
	}
	public override float roll {
		get { return values.y; }
	}
	public override float pitch {
		get { return values.z; }
	}
	public override float yaw {
		get { return values.w; }
	}
	
	void Update()
	{
		values.x = Input.GetAxis (AxisNameThrottle);
		values.y = Input.GetAxis (AxisNameRoll);
		values.z = Input.GetAxis (AxisNamePitch);
		values.w = Input.GetAxis (AxisNameYaw);
	}
}
