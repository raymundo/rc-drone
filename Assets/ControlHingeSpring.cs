﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(HingeJoint))]
public class ControlHingeSpring : MonoBehaviour
{	
	public ControlSignalsMultiplexer controlSignals;
	public Vector4 signalWeights;
	public float StabilizationFactor = 0.0f;
	
	private Vector3 initialLocalGravityOnAxis;
	private Vector3 initialLocalGravity;
	
	void Start() {
		initialLocalGravity = transform.InverseTransformDirection(Physics.gravity);
		initialLocalGravityOnAxis = initialLocalGravity - hingeJoint.axis * Vector3.Dot(hingeJoint.axis, initialLocalGravity);
	}

	void FixedUpdate ()
	{
		float inputValue = Vector4.Dot(controlSignals.GetOutputs(), signalWeights);
		float targetValue = Mathf.Lerp (hingeJoint.limits.min, hingeJoint.limits.max, inputValue * 0.5f + 0.5f);
		
		Vector3 localGravity = transform.InverseTransformDirection(Physics.gravity);
		Vector3 localGravityOnAxis = localGravity - hingeJoint.axis * Vector3.Dot(hingeJoint.axis, localGravity);
		//float diffAngle = Vector3.Angle(initialLocalGravityOnAxis, localGravityOnAxis);
		Quaternion desiredRotation = Quaternion.FromToRotation(localGravityOnAxis, initialLocalGravityOnAxis);
		float desiredRotationAngle;
		Vector3 desiredRotationAxis;
		desiredRotation.ToAngleAxis(out desiredRotationAngle, out desiredRotationAxis);
		if (Vector3.Dot(desiredRotationAxis, hingeJoint.axis) < 0) {
			desiredRotationAxis *= -1;
			desiredRotationAngle *= -1;
		}

		//Debug.Log(desiredRotationAngle);
		//Debug.DrawRay(transform.position, transform.TransformDirection(initialLocalGravityOnAxis));
		//Debug.DrawRay(transform.position, transform.TransformDirection(localGravityOnAxis));
		//Debug.DrawRay(transform.position, localGravityOnAxis);
		
		JointSpring spring = hingeJoint.spring;
		spring.targetPosition = targetValue;
		spring.targetPosition -= StabilizationFactor * desiredRotationAngle;
		hingeJoint.spring = spring;
	}
}
