﻿using UnityEngine;
using System.Collections;

public class MatrixMixer : AbstractFilter {
	
	public Matrix4x4 MixMatrix = Matrix4x4.identity;

	protected override Vector4 Outputs {
		get {
			return MixMatrix * new Vector4(
			inputs.throttle,
			inputs.roll,
			inputs.pitch,
			inputs.yaw
			);
		}
	}
}
