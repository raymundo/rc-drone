﻿using UnityEngine;
using System.Collections;

public class DualJoystickInputSampler : ControlSignals {
	
	public Joystick left;
	public Joystick right;
	
	private Vector4 values;

	public override float throttle {
		get { return values.x; }
	}
	public override float roll {
		get { return values.y; }
	}
	public override float pitch {
		get { return values.z; }
	}
	public override float yaw {
		get { return values.w; }
	}
	
	void Update()
	{
		values.x = left.position.y;
		values.y = right.position.x;
		values.z = right.position.y;
		values.w = left.position.x;
	}
}
