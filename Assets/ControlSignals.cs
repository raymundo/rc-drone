﻿using UnityEngine;
using System.Collections;

public abstract class ControlSignals : MonoBehaviour {
	public abstract float throttle { get; }
	public abstract float roll { get; }
	public abstract float pitch { get; }
	public abstract float yaw { get; }
}
