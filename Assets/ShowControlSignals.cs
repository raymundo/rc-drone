﻿using UnityEngine;
using System.Collections;

public class ShowControlSignals : AbstractFilter
{
	protected override Vector4 Outputs {
		get {
			return new Vector4 (
			inputs.throttle,
			inputs.roll,
			inputs.pitch,
			inputs.yaw
			);
		}
	}
}
