﻿using UnityEngine;
using System.Collections;

public class ResetOrientation : MonoBehaviour {

	public string ButtonName = "Fire1";
	
	void FixedUpdate () {
		if (Application.platform == RuntimePlatform.Android) {
			return;
		}
		if (Input.GetButton(ButtonName)) {
			rigidbody.MoveRotation(Quaternion.identity);
			rigidbody.angularVelocity = Vector3.zero;
			rigidbody.velocity = Vector3.zero;
		}
	}
	void OnGUI()
	{
		GUILayout.BeginArea(new Rect(0,0,Screen.width, Screen.height));
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		if (GUILayout.Button("Reset Orientation")) {
			rigidbody.MoveRotation(Quaternion.identity);
			rigidbody.angularVelocity = Vector3.zero;
			rigidbody.velocity = Vector3.zero;
		}
		GUILayout.EndHorizontal();
		GUILayout.EndArea();
	}
}
