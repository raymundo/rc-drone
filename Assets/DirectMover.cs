﻿using UnityEngine;
using System.Collections;

public class DirectMover : MonoBehaviour {

	public float VelocityScale = 5.0f;
	public float RotationScale = 50.0f;
	
	public ControlSignalsMultiplexer controlSignals = null;
	
	// Update is called once per frame
	void FixedUpdate () {
		float up = controlSignals.throttle;
		float forward = controlSignals.pitch;
		float right = controlSignals.roll;
		float rotateRight = controlSignals.yaw;
		rigidbody.MovePosition(transform.position + Time.deltaTime * (Vector3.forward * forward + Vector3.up * up + Vector3.right * right) * VelocityScale);
		rigidbody.MoveRotation(rigidbody.rotation * Quaternion.AngleAxis(Time.deltaTime * rotateRight * RotationScale, Vector3.up));
	}
}
