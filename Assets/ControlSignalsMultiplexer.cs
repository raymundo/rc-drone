﻿using UnityEngine;
using System;

public class ControlSignalsMultiplexer : ControlSignals
{
	public ShowControlSignals[] alternatives;

	protected Vector4 Outputs {
		get {
			return new Vector4 (
			alternatives[0].throttle,
			alternatives[0].roll,
			alternatives[0].pitch,
			alternatives[0].yaw
			);
		}
	}

	public Vector4 GetOutputs() {
		return Outputs;
	}

	public override float throttle {
		get { return Outputs.x; }
	}
	public override float roll {
		get { return Outputs.y; }
	}
	public override float pitch {
		get { return Outputs.z; }
	}
	public override float yaw {
		get { return Outputs.w; }
	}

	public string CycleButtonName = "Jump";
	
	void Update()
	{
		if (Application.platform == RuntimePlatform.Android) {
			return;
		}
		if (alternatives.Length == 0) {
			return;
		}
		if (Input.GetButtonDown(CycleButtonName)) {
			ShowControlSignals temp = alternatives[0];
			Array.Copy(alternatives, 1, alternatives, 0, alternatives.Length - 1);
			alternatives[alternatives.Length - 1] = temp;
		}
	}
	
	void OnGUI()
	{
		if (GUILayout.Button(alternatives[0].name)) {
			if (alternatives.Length == 0) {
				return;
			}
			ShowControlSignals temp = alternatives[0];
			Array.Copy(alternatives, 1, alternatives, 0, alternatives.Length - 1);
			alternatives[alternatives.Length - 1] = temp;
		}
	}
}
